# pylint: disable=missing-docstring,invalid-name
from cpt.packager import ConanMultiPackager

if __name__ == "__main__":
    builder = ConanMultiPackager()
    builder.add_common_builds(shared_option_name="cfitsio:shared",
                              pure_c=False)
    builder.run()
