from conans import CMake, ConanFile, tools


class CfitsioConan(ConanFile):
    name = "cfitsio"
    version = "3.450"
    license = "MIT"
    author = "NASA"
    url = "https://gitlab.com/mdavezac-conan/cfitsio"
    homepage = "https://heasarc.gsfc.nasa.gov/fitsio/"
    tar_url = "http://heasarc.gsfc.nasa.gov/FTP/software/fitsio/c/cfitsio3450.tar.gz"
    description = (
        "C and Fortran library for reading and writing data files in FITS format"
    )
    topics = ("Physics", "AstroPhysics", "Imaging")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"

    def source(self):
        tools.download(self.tar_url, "cfitsio.tar.gz")
        tools.untargz("cfitsio.tar.gz")

    def configured_cmake(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_DISABLE_FIND_PACKAGE_CURL"] = True
        cmake.definitions["USE_PTHREADS"] = False
        cmake.definitions["CMAKE_POSITION_INDEPENDENT_CODE"] = True
        cmake.configure(source_folder="cfitsio")
        return cmake

    def build(self):
        cmake = self.configured_cmake()
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="cfitsio")
        self.copy("*cfitsio.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.so.*", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["cfitsio"]
