cmake_minimum_required(VERSION 3.7)

project(ssht_test C)


set(CMAKE_C_STANDARD 99)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup(TARGETS KEEP_RPATHS)

IF (MSVC OR MINGW)
    SET(M_LIB "")
ELSE()
    FIND_LIBRARY(M_LIB m)
ENDIF()

add_executable(cookbook cookbook.c)
target_link_libraries(cookbook CONAN_PKG::cfitsio CONAN_LIB::cfitsio_cfitsio
  ${M_LIB})
